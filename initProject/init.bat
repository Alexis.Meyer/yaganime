@echo off
setlocal

REM Verifier si Python est installe
python --version >nul 2>&1
if %errorlevel% neq 0 (
    echo Python n'est pas installe. Veuillez l'installer avant d'executer ce script.
    exit /b 1
)

REM Creer un environnement virtuel nomme "env"
python -m venv ../env

REM Activer l'environnement virtuel
call ..\env\Scripts\activate

REM Choix de la base de donnee
set /p BDD=Si votre base de donnee est mysql entrez 1 si elle est postgres entrez 2 :

REM Verifier si pip est disponible
where pip >nul 2>&1
if %errorlevel% neq 0 (
    echo Erreur : pip n'est pas installé. Veuillez installer Python avec pip avant d'exécuter ce script.
    exit /b 1
)

REM Installer les bibliotheques Python requises
if %BDD% neq 0 (
    pip install django python-dotenv django-bootstrap-v5 django-crispy-forms requests mysqlclient
)
else (
    pip install django python-dotenv django-bootstrap-v5 django-crispy-forms requests psycopg2
)

echo Environnement Python créé avec succès et les bibliothèques ont été installées.


REM Demander a l'utilisateur de saisir les valeurs
python scriptEnv.py

REM Migration de la base de donnée
python ..\src\yaganime\debugRun.py migrate

python .\scriptSUCreate.py