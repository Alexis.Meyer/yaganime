import subprocess

createSuperUser = "gateau"

while(createSuperUser != "oui" and createSuperUser != "non") :
    createSuperUser = input("Voulez-vous créer un super user ? (oui/non) : ")

if(createSuperUser == "oui") :
    cheminScript = "..\src\yaganime\debugRun.py"

    subprocess.run(["python", cheminScript, "createsuperuser"])
