# Demander à l'utilisateur de saisir les valeurs
db_engine = input("Entrez la valeur de DBENGINE ('mysql' ou 'postgresql') : ")
db_name = input("Entrez la valeur de DBNAME : ")
db_user = input("Entrez la valeur de DBUSER : ")
db_password = input("Entrez la valeur de DBPASSWORD (attention visible) : ")
db_host = input("Entrez la valeur de DBHOST : ")
db_port = input("Entrez la valeur de DBPORT : ")

# Écrire les valeurs dans le fichier .env
with open("..\src\yaganime\yaganime\settings\.env", "w") as env_file:
    env_file.write(f"DBENGINE=django.db.backends.{db_engine}\n")
    env_file.write(f"DBNAME={db_name}\n")
    env_file.write(f"DBUSER={db_user}\n")
    env_file.write(f"DBPASSWORD={db_password}\n")
    env_file.write(f"DBHOST={db_host}\n")
    env_file.write(f"DBPORT={db_port}\n")

print("Les valeurs ont été enregistrées dans le fichier .env avec succès.\n")
