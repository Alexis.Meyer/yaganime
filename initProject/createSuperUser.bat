@echo off
setlocal

REM Verifier si Python est installe
python --version >nul 2>&1
if %errorlevel% neq 0 (
    echo Python n'est pas installe. Veuillez l'installer avant d'executer ce script.
    exit /b 1
)

python .\scriptSUCreate.py