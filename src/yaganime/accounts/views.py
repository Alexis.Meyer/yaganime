from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login
from .forms import RegisterForm

def sign_up(request):
    
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit = False)
            user.save()
            print(" world")
            messages.success(request, "Votre compte a été créé !")
            login(request, user)
            return redirect('/')
    else:
        form = RegisterForm()
    return render(request , 'registration/register.html' , {'form': form})
