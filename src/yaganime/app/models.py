# Importation de la classe de modèle (models) depuis le module django.db
from django.db import models

# Importation de la classe User depuis le module django.contrib.auth.models
from django.contrib.auth.models import User


# Classe de modèle pour la liste d'animes personnalisée
class ListesAnimes(models.Model):
    # Relation OneToOne avec le modèle User pour associer chaque liste à un utilisateur
    id_user = models.OneToOneField(User, on_delete=models.CASCADE)

    # Champ pour stocker le nom de la liste d'animes
    name_list = models.CharField(max_length=100)

    # Champ pour stocker l'identifiant de l'anime
    id_anime = models.ManyToManyField('Animes')

    objects = models.Manager()

    class Meta:
        # Définit que la combinaison de "id_user" et "name_list" doit être unique
        unique_together = ("id_user", "name_list")


# Classe de modèle pour les informations des animes
class Animes(models.Model):
    # Champ pour stocker l'identifiant de l'anime (clé primaire)
    id_anime = models.AutoField(primary_key=True)

    # Champ pour stocker le titre de l'anime
    titre = models.CharField(max_length=255)

    # Champ pour stocker la note moyenne de l'anime
    mean_score = models.FloatField()

    # Champ pour stocker le rang de l'anime
    rang = models.IntegerField()

    # Champ pour stocker le chemin de l'image de l'anime
    image = models.CharField(max_length=255)

    objects = models.Manager()
