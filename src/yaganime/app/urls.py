# Importation de la classe de chemin (path) depuis le module django.urls
from django.urls import path

# Importation de toutes les vues (views) depuis le même répertoire
from . import views

# Liste des URL de l'application
urlpatterns = [
    # Chemin URL pour la page d'accueil (vue index)
    path("", views.index, name='accueil'),

    # Chemin URL pour la page de connexion (vue login)
    path("login/", views.login, name='login-perso'),

    # Chemin URL pour la page de liste personnalisée d'animes (vue my_list)
    path("myList/", views.my_list, name='myList'),

    # Chemin URL pour la page du profil de l'utilisateur (vue user)
    path("compte/", views.user, name='compte'),

    # Chemin URL pour la page de recherche d'animes (vue find)
    path("find/", views.find, name='find'),

    # Chemin URL pour la page "À propos" (vue about)
    path("about/", views.about, name='about'),

    # Chemin URL pour la page de détails d'un anime spécifique (vue anime_details)
    # Le "<int:anime_id>/" permet de capturer un entier et de le transmettre à la vue comme "anime_id"
    path("details/<int:anime_id>/", views.anime_details, name='details'),
    path('ajouter_liste_anime/', views.ajouter_liste_anime, name='ajouter_liste_anime'),
    path('supprimer_anime_from_list/', views.supprimer_anime_from_list, name='supprimer_anime_from_list'),
    path('create_list/', views.create_list, name='create_list'),

]
