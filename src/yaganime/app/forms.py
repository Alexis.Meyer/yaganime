# Importation de la classe de formulaire (forms) depuis le module django
from django import forms

# Définition d'un formulaire de recherche d'anime
class AnimeSearch(forms.Form):
    # Champ de formulaire pour la saisie du nom de l'anime
    Anime = forms.CharField()
