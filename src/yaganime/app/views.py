# Importation du modèle personnalisé ListesAnimes du module .models
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

from .models import ListesAnimes

# Importation de la fonction render depuis le module django.shortcuts
from django.shortcuts import render, redirect

# Importation de la classe de formulaire AnimeSearch depuis le module .forms
from .forms import AnimeSearch
# views.py
from django.http import JsonResponse
from .models import Animes  # Importez le modèle Anime

# Importation du module requests pour effectuer des requêtes HTTP
import requests


# Vue pour la page d'accueil
def index(request):
    anime_season = []
    top_anime = []
    top_all_time = []
    headers = {
        "X-MAL-CLIENT-ID": "b6b812db63c497e20ea09c841744d415"
    }
    anime_season_response = requests.get(
        "https://api.myanimelist.net/v2/anime/season/2023/summer?limit=5",
        headers=headers)
    top_anime_response = requests.get(
        "https://api.myanimelist.net/v2/anime/ranking?ranking_type=upcoming&limit=5",
        headers=headers)
    top_all_time_response = requests.get(
        "https://api.myanimelist.net/v2/anime/ranking?ranking_type=bypopularity&limit=5",
        headers=headers)
    anime_season_data = anime_season_response.json()
    top_anime_data = top_anime_response.json()
    top_all_time_data = top_all_time_response.json()
    for anime_entry in anime_season_data.get("data", []):
        anime_id = anime_entry["node"]["id"]
        name = anime_entry["node"]["title"]
        if "mean" in anime_entry:
            mean = anime_entry["mean"]
        else:
            mean = "NE"
        if "rank" in anime_entry:
            rank = anime_entry["rank"]
        else:
            rank = "Unranked"
        anime_season_picture = anime_entry["node"]["main_picture"]["medium"]
        anime_season.append({"id": anime_id, "name": name, "picture": anime_season_picture, "mean": mean, "rank": rank})
    for anime_entry in top_anime_data.get("data", []):
        anime_id = anime_entry["node"]["id"]
        name = anime_entry["node"]["title"]
        if "mean" in anime_entry:
            mean = anime_entry["mean"]
        else:
            mean = "NE"
        if "rank" in anime_entry:
            rank = anime_entry["rank"]
        else:
            rank = "Unranked"
        top_anime_picture = anime_entry["node"]["main_picture"]["medium"]
        top_anime.append({"id": anime_id, "name": name, "picture": top_anime_picture, "mean": mean, "rank": rank})
    for anime_entry in top_all_time_data.get("data", []):
        anime_id = anime_entry["node"]["id"]
        name = anime_entry["node"]["title"]
        if "mean" in anime_entry:
            mean = anime_entry["mean"]
        else:
            mean = "NE"
        if "rank" in anime_entry:
            rank = anime_entry["rank"]
        else:
            rank = "Unranked"
        top_all_time_picture = anime_entry["node"]["main_picture"]["medium"]
        top_all_time.append({"id": anime_id, "name": name, "picture": top_all_time_picture, "mean": mean, "rank": rank})
    return render(
        request,
        "accueil.html",
        context={"anime_season": anime_season, "top_anime": top_anime, "top_all_time": top_all_time})


# Vue pour la page de connexion (login)
def login(request):
    return render(request, "login.html")


# Vue pour afficher la liste d'animes personnalisée de l'utilisateur
def my_list(request):
    user = request.user

    try:
        liste_animes = ListesAnimes.objects.get(id_user=user)
        has_liste = True
        list_name = liste_animes.name_list
        list_animes = liste_animes.id_anime.all()
        list_length = list_animes.count()
    except ObjectDoesNotExist:
        liste_animes = None
        has_liste = False
        list_name = ""
        list_animes = []
        list_length = 0

    list_context = []
    has_anime = False

    for anime in list_animes:
        anime_id = anime.id_anime
        name = anime.titre
        image = anime.image
        mean = anime.mean_score
        rank = anime.rang
        list_context.append({
            "id": anime_id,
            "title": name,
            "picture": image,
            "mean": mean,
            "rank": rank,
        })
        has_anime = True

    context = {
        "listAnimes": list_context,
        "has_list": has_liste,
        "hasAnime": has_anime,
        "list_name": list_name,
        "list_length": list_length,
    }

    return render(request, "myList.html", context)


@login_required
def create_list(request):
    if request.method == 'POST':
        list_name = request.POST.get('list_name')
        user = request.user
        new_list = ListesAnimes(id_user=user, name_list=list_name)
        new_list.save()
        # Redirigez l'utilisateur vers la page de sa liste nouvellement créée
        return redirect('myList')

    return JsonResponse({'message': 'Liste ajouté avec succès!'})


# Vue pour la page du profil de l'utilisateur
def user(request):
    return render(request, "compte.html")


# Vue pour la recherche d'animes
def find(request):
    # Créer une instance du formulaire AnimeSearch
    form = AnimeSearch()

    if request.method == "POST":
        # Si la requête est de type POST, cela signifie que le formulaire a été soumis
        form = AnimeSearch(request.POST)
        if form.is_valid():
            # Utilisation de l'API MyAnimeList pour rechercher des animes
            anime_data = []
            anime_name = form.cleaned_data["Anime"]
            headers = {
                "X-MAL-CLIENT-ID": "b6b812db63c497e20ea09c841744d415"
            }
            response = requests.get("https://api.myanimelist.net/v2/anime?q=" + anime_name, headers=headers)
            data = response.json()
            for anime_entry in data.get("data", []):
                id = anime_entry["node"]["id"]
                name = anime_entry["node"]["title"]
                response2 = requests.get("https://api.myanimelist.net/v2/anime/" + str(id) + "?fields=mean,rank",
                                         headers=headers)
                json_data = response2.json()
                if "mean" in json_data:
                    mean = json_data["mean"]
                else:
                    mean = "NE"
                if "rank" in json_data:
                    rank = json_data["rank"]
                else:
                    rank = "Unranked"
                picture = anime_entry["node"]["main_picture"]["medium"]
                anime_data.append({"id": id, "name": name, "picture": picture, "mean": mean, "rank": rank})

            # Rendre la page de résultats de recherche
            return render(request, "rechercher.html",
                          context={"anime_data": anime_data, "data": data, "json_data": json_data, "form": form})

    # Si la requête est de type GET ou que le formulaire n'est pas valide, afficher le formulaire de recherche
    return render(request, "rechercher.html", context={"form": form})


# Vue pour afficher les détails d'un anime spécifique
def anime_details(request, anime_id):
    headers = {
        "X-MAL-CLIENT-ID": "b6b812db63c497e20ea09c841744d415"
    }
    anime_data = []
    response2 = requests.get(
        "https://api.myanimelist.net/v2/anime/" + str(anime_id) + "?fields=title, main_picture, start_date, end_date,"
                                                                  "synopsis, mean, rank, status, "
                                                                  "genres, num_episodes, "
                                                                  "average_episode_duration, "
                                                                  "studios",
        headers=headers)
    json_data = response2.json()
    if "mean" in json_data:
        mean = json_data["mean"]
    else:
        mean = "NE"
    if "rank" in json_data:
        rank = json_data["rank"]
    else:
        rank = "Unranked"
    if "start_date" in json_data:
        start_date = json_data["start_date"]
    else:
        start_date = "NE"
    if "end_date" in json_data:
        end_date = json_data["end_date"]
    else:
        end_date = "NE"
    if "synopsis" in json_data:
        synopsis = json_data["synopsis"]
    else:
        synopsis = "NE"
    if "status" in json_data:
        status = json_data["status"]
    else:
        status = "NE"
    if "genres" in json_data:
        genres = json_data["genres"]
    else:
        genres = "NE"
    if "num_episodes" in json_data:
        num_episodes = json_data["num_episodes"]
    else:
        num_episodes = "NE"
    if "average_episode_duration" in json_data:
        seconds = json_data["average_episode_duration"]

        seconds = seconds % (24 * 3600)
        hour = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60
        if hour > 0:
            average_episode_duration = f'{hour}h{minutes}min{seconds}s'
        else:
            average_episode_duration = f'{minutes}min{seconds}s'
    else:
        average_episode_duration = "NE"
    if "studios" in json_data:
        studios = json_data["studios"]
    else:
        studios = "NE"
    if "title" in json_data:
        title = json_data["title"]
    else:
        title = "NE"
    if "studios" in json_data:
        main_picture = json_data["main_picture"]
    else:
        main_picture = "NE"
    anime_data.append({
        "id": anime_id,
        "title": title,
        "picture": main_picture,
        "mean": mean,
        "rank": rank,
        "start_date": start_date,
        "end_date": end_date,
        "synopsis": synopsis,
        "status": status,
        "genres": genres,
        "num_episodes": num_episodes,
        "average_episode_duration": average_episode_duration,
        "studios": studios,
    })

    # Rendre la page des détails de l'anime
    return render(request, "animeDetails.html", context={"anime_data": anime_data})


# Vue pour la page "À propos"
def about(request):
    return render(request, "about.html")


@login_required
def ajouter_liste_anime(request):
    if request.method == 'POST':
        anime_id = request.POST.get('anime_id')
        name = request.POST.get('name')
        picture = request.POST.get('picture')
        mean = request.POST.get('mean')
        rank = request.POST.get('rank')
        user = request.user

        try:
            anime = Animes.objects.get(id_anime=anime_id)
        except ObjectDoesNotExist:
            anime = Animes.objects.create(id_anime=anime_id, titre=name, mean_score=mean, rang=rank, image=picture)
        try:
            liste_animes = ListesAnimes.objects.get(id_user=user)
        except ObjectDoesNotExist:
            return redirect('myList')
        liste_animes.id_anime.add(anime)
        liste_animes.save()

        return redirect('myList')

    return JsonResponse({'message': 'Anime ajouté avec succès!'})


@login_required
def supprimer_anime_from_list(request):
    if request.method == 'POST':
        anime_id = request.POST.get('anime_id')
        user = request.user
        # Obtenir la liste de l'utilisateur
        liste_animes = ListesAnimes.objects.get(id_user=user)
        # Supprimer l'anime de la liste
        liste_animes.id_anime.remove(anime_id)
        liste_animes.save()
        return redirect('myList')

    return JsonResponse({'message': 'Requête non valide.'})
