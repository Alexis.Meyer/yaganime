Lancer projet:
- `.\env\Scripts\activate`
- `cd src`
- `make`

Mettre à jour la base de données:
- `.\env\Scripts\activate`
- `cd src`
- `cd yaganime`
- `python degubRun.py makemigrations`
- `python debugRun.py migrate`

Pour initialiser le projet :
- `.\initProjetct\init.bat`